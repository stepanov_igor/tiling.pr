#!/usr/bin/python3

import os
import socket
import select
import time
import json
import argparse
import platform
import random
import re

path = "./"

for root, d_names, f_names in os.walk(path):
    for fname in f_names:
        if fname.lower().endswith(('.svg')) and not './pix/exp/' in root:
            name = (root + '/' + fname)
            #print(root)
            print(name)
            name = os.path.splitext(name)[0]
            s = 'inkscape -D "' + name + '.svg" -o "' + name + '.pdf" --export-latex &'
            #print(s)
            os.system(s)
